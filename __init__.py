"""
data
  起源, 树种组成, 平均胸径

"""

import re
import csv


class Tree:

    def __init__(self):
        self.price_dict = Tree.gen_price_dict()

    @staticmethod
    def gen_price_dict():
        """生成价格字典

        :return: {'白桦-天然林-14': '4.7109984', '白桦-天然林-16': '5.5429728', '白桦-天然林-18': '6.0177408', ....}
        """
        price_dict = {}
        with open('./data/price.csv', 'r', encoding='utf-8') as f:
            spamreader = csv.reader(f, delimiter=',')
            for row in spamreader:
                # name, kind, diameter, price = row[0:4]
                k = '-'.join(row[0:3])
                price_dict[k] = float(row[3])
        return price_dict

    def cal(self):
        # 这里修改为对应年份数据路径
        with open('./data/2010.csv', 'r', encoding='utf-8') as f:
            spamreader = csv.reader(f, delimiter=',')
            for row in spamreader:
                kind, line, diameter = row[0:3]

                if line:
                    r = re.split('(\d+)', line)
                    r = r[1:]
                    data = dict(zip(r[1::2], r[::2]))  # 树的名字在前, 数字会重复，不可做key
                    res = dict(map(lambda d: (d[0], self.price_dict.get('-'.join([d[0], kind, diameter.strip()]), 0) * int(d[1])), data.items()))

                    # 输出格式: 原始行信息， 树种价格组成，行内价格合计
                    print('{}, {}, {}'.format(','.join(row), Tree.dict2str(res), sum(res.values())))
                else:
                    print('{}, {}, {}'.format(','.join(row), '', 0))

    @staticmethod
    def dict2str(d):
        return '，'.join("%s:%s" % (k, v) for k, v in d.items())


if __name__ == '__main__':
    t = Tree()
    # print(t.price_dict)
    t.cal()
